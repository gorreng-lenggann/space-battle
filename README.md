# S.P.A.C.E. Battle 


### Game Description
> S.P.A.C.E. Battle is a game made by 5 CSUI 2017 Student for their gamedev project. S.P.A.C.E. Battle is a 2 Player offline game which takes heavy inspiration from Pong and Lethal League

### Lore 

> Year 51532 A.D., when humans have transcended Godhood. The human managed to build Dyson Spheres around the super clusters and harness the power of multiple galaxies. When it seem there's nothing that can stop human's expansion of power exploitation, another lifeform that can also harness the galaxies' power strikes for dominance. They claim themselves as "Bauthynr Adiccr Neic G'ver Ssynec Arhynr Tymo" race, or "B.G.S.T." as the human call them, and they demand human's submission to their superior being. Agitated, the humans fought the "B.G.S.T." in this epic marvelous divine incomparable galaxy....